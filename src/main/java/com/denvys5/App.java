package com.denvys5;


import com.denvys5.serial.ShutdownController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

public class App extends Application {
    public static void main( String[] args ) {
        launch(args);
    }

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void start(Stage stage) throws Exception {
        preInitialization();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/mainUI.fxml"));
        Parent root = fxmlLoader.load();

        stage.setTitle("Serial Debug Application");
        stage.setScene(new Scene(root));
        stage.setOnCloseRequest((t) -> ShutdownController.onButtonPress());

        CompletableFuture.runAsync(this::initialization);
        uiInitialization();

        stage.show();
    }

    private void preInitialization(){
        log.info("Pre-Initialization");
    }

    private void uiInitialization(){
        log.info("UI-Initialization");
    }

    private void initialization(){
        log.info("Initialization");
    }
}
