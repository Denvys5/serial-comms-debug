package com.denvys5.serial.controller;

import com.denvys5.config.ConfigFactory;
import com.denvys5.config.KeyValueConfig;
import com.denvys5.serial.ISerialConfigController;

public class ConfigController implements ISerialConfigController {
    private KeyValueConfig properties;

    private int serialCommunicationSpeed;
    private int serialTimeForACK;
    private int serialClearAcksTimeDelay;
    private int serialSendMessagesTimeDelay;
    private int serialPortScanDelay;
    private int serialWaitForDevice;
    private int serialWaitForDeviceCycles;
    private int serialConnectionTimeout;
    private boolean rawMessages;
    private boolean debug;

    public void serialSetup(){
        properties = ConfigFactory.getPropertiesConfig("system");

        debug = properties.getPropertyBoolean("debug", false);
        rawMessages = properties.getPropertyBoolean("serial.rawMessages", true);
        serialCommunicationSpeed = properties.getPropertyInt("serial.communicationSpeed", 9600);
        serialTimeForACK = properties.getPropertyInt("serial.timeForACK", 500);
        serialClearAcksTimeDelay = properties.getPropertyInt("serial.clearAcksTimeDelay", 5000);
        serialSendMessagesTimeDelay = properties.getPropertyInt("serial.sendMessagesTimeDelay", 100);
        serialPortScanDelay = properties.getPropertyInt("serial.portScanDelay", 1000);
        serialWaitForDevice = properties.getPropertyInt("serial.waitForDevice", 2000);
        serialWaitForDeviceCycles = properties.getPropertyInt("serial.waitForDeviceCycles", 4);
        serialConnectionTimeout = properties.getPropertyInt("serial.connectionTimeout", 60000);
    }

    public boolean isDebug() {
        return debug;
    }

    public boolean isRawMessages() {
        return rawMessages;
    }

    public int getSerialCommunicationSpeed() {
        return serialCommunicationSpeed;
    }

    public int getSerialTimeForACK() {
        return serialTimeForACK;
    }

    public int getSerialClearAcksTimeDelay() {
        return serialClearAcksTimeDelay;
    }

    public int getSerialSendMessagesTimeDelay() {
        return serialSendMessagesTimeDelay;
    }

    public int getSerialWaitForDevice() {
        return serialWaitForDevice;
    }

    public int getSerialWaitForDeviceCycles() {
        return serialWaitForDeviceCycles;
    }

    public int getSerialPortScanDelay() {
        return serialPortScanDelay;
    }

    public int getSerialConnectionTimeout() {
        return serialConnectionTimeout;
    }
}
