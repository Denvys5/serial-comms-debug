package com.denvys5.serial.controller;

import com.denvys5.serial.SerialController;

public class CommunicationController {
    private SerialController serialController;
    private ConfigController configController;

    public void init(){
        configController = new ConfigController();

        serialController = new SerialController();
        serialController.setConfigController(configController);
        serialController.init();

        serialController.getSerialProtocolMessageListeners().addByteArraySentListener(this::onByteArraySent);
        serialController.getSerialProtocolMessageListeners().addByteArrayReceivedListener(this::onByteArrayReceived);
    }

    private void onByteArraySent(byte[] message){

    }

    private void onByteArrayReceived(byte[] message){

    }
}
